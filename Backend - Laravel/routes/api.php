<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->post('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });
        


        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
        //Only login users has access below routes
        $api->post('book', 'App\\Api\\V1\\Controllers\\BookingController@bookbus');
        $api->post('GetBookedSeats', 'App\\Api\\V1\\Controllers\\BookingController@GetBookedSeats');
        // $api->post('GetBookedSeats', 'App\\Api\\V1\\Controllers\\BookingController@GetBookedSeats');
        $api->post('cancelBooking', 'App\\Api\\V1\\Controllers\\BookingController@cancelBooking');
        $api->post('GetUserBookings', 'App\\Api\\V1\\Controllers\\UserController@GetUserBookings');
        $api->post('GetAllBookings', 'App\\Api\\V1\\Controllers\\AdminController@GetAllBookings');
        $api->post('setPaid', 'App\\Api\\V1\\Controllers\\AdminController@setPaid');
        $api->post('GetAllTravels', 'App\\Api\\V1\\Controllers\\AdminController@GetAllTravels');
        
        $api->post('createTravel', 'App\\Api\\V1\\Controllers\\AdminController@createTravel');
        $api->post('editTravel', 'App\\Api\\V1\\Controllers\\AdminController@editTravel');
        $api->post('deleteTravel', 'App\\Api\\V1\\Controllers\\AdminController@deleteTravel');
    $api->post('viewTravel', 'App\\Api\\V1\\Controllers\\TravelController@viewTravel');
    });
    
    
    //Everyone has access below routes
    $api->get('viewTravels', 'App\\Api\\V1\\Controllers\\TravelController@viewTravelsList');
    $api->get('viewBus/{id}', 'App\\Api\\V1\\Controllers\\TravelController@viewBusDetails');
    $api->post('filterBus', 'App\\Api\\V1\\Controllers\\TravelController@filterBus');
    $api->get('activeApp/{id}', 'App\\Api\\V1\\Controllers\\TravelController@activeApp');
    $api->post('setLocation', 'App\\Api\\V1\\Controllers\\TravelController@setLocation');
});
