<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    public function travel(){
        return $this->belongsTo('App\Travel','vehicle_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
