<?php

namespace App\Api\V1\Controllers;
require base_path().'/vendor/twilio-php-master/Twilio/autoload.php';

use Twilio\Rest\Client;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use Illuminate\Http\Request;
use App\Bookings;
use App\Travel;

class BookingController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function bookbus(Request $request)
    {
        // return $request;
        $Book=new Bookings();
        $Book->vehicle_id=$request->input('travel_id');
        $Book->user_id=$request->input('userid');
        $Book->date=$request->input('date');
        $Book->time=$request->input('time');
        $Book->payment_status='unpaid';
        $Book->payment_type=$request->input('payment_type');
        $Book->amount=$request->input('amount');
        $Book->seat_numbers=$request->input('seat_numbers');

        $vehicle_id=$Book->vehicle_id;
        
        $travel=Travel::select('name')->where('id',$vehicle_id)->get();
        $travel_name= $travel[0]['name'];
        if($Book->save()){
            // return $request;
            $mes=$this->sendMessage($travel_name,$Book->seat_numbers,$Book->date,$Book->time,$Book->amount);
            // return $mes;
            if($mes){
                return response()->json(['message'=>'Successfully Booked','status'=>'200'],200);
            }

        }else{
            return response()->json(['message'=>'Successfully Booked','status'=>'406'],400);
        }

        // return "BookBus";
    }

    public function CancelBooking(Request $request){
        $booking_id=$request->only('booking_id');
        $user_id=Auth::user()->id;
        $booking=Bookings::select()->where('user_id',$user_id)->where('id',$booking_id);
        if($booking->delete()){
            $this->CancelMessage();
            return response()->json(['message'=>'Booking successfully cancelled','status'=>'200'],200);
            
        }else{
            return response()->json(['message'=>'Cancellation failed','status'=>'201'],201);
        }
    }

    public function GetBookedSeats(Request $request){
        $vehicle_id=$request->only('vehicle_id');
        $date=$request->only('date');
        $bookings=Bookings::select()->where('vehicle_id',$vehicle_id)->where('date',$date)->get();
        // return $bookings
        $booked_seats=[];
        for($x=0;$x<$bookings->count();$x++){
            $seats= json_decode($bookings[$x]['seat_numbers']);
            for($y=0;$y<count($seats);$y++){
                $arr=array($seats);
                $Books=array_push($booked_seats,$seats[$y]);
            }
        }
        // if(count($booked_seats)!=0){
            return response()->json(['seats'=>$booked_seats,'status'=>'200'],200);
        // }else{
            // return response()->json(['seats'=>[],'status'=>'201'],201);
        // }

    }
    public function sendMessage($travel_name,$seat_numbers,$date,$time,$cost){
        $sid = env('sid');
        $token = env('token');
        $client = new Client($sid, $token);
        // return $client;
        $msg="\nTravel:".$travel_name.",\nDate: ".$date.",\nTime: ".$time."\nSeats:".$seat_numbers."\nCost:".$cost.",\nTel:0778514083,1\nluggage & bag only";
        // return $msg;

        $client->messages->create(
            // the number you'd like to send the message to
            env('sendTo'),
            // '+94770723090',
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => env('sendFrom'),
                // the body of the text message you'd like to send
                'body' => $msg
            )
        );
        return true;
    }
    public function CancelMessage(){
        $sid = env('sid');
        $token = env('token');
        $client = new Client($sid, $token);
        // return $client;
        $msg="Your booking is cancelled";
        $client->messages->create(
            // the number you'd like to send the message to
            env('sendTo'),
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => env('sendFrom'),
                // the body of the text message you'd like to send
                'body' => $msg
            )
        );
    }
}
