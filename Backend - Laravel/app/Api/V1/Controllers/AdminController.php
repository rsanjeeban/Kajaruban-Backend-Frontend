<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use Illuminate\Http\Request;
use App\Bookings;
use App\Travel;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(Auth::guard()->user());
    }

    public function GetAllBookings(Request $request){
        $usertype=Auth::user()->type;
        if($usertype=='admin'){
            $bookings=Bookings::with('travel')->with('user:id,name,phone,email')->get();
            return $bookings;
        }
        // $userid=$request->only('user_id');
        
    }
    public function GetAllTravels(Request $request){
        $usertype=Auth::user()->type;
        if($usertype=='admin'){
            // $userid=$request->only('user_id');
            $travels=Travel::all();
            return $travels;
        }
        
        
    }
    public function setPaid(Request $request){
        $usertype=Auth::user()->type;
        if($usertype=='admin'){
            $user_id=$request->only('user_id');
            $booking_id=$request->only('booking_id');
            $paid_status=$request->only('paid_status');
            // return ;
            // $booking=Bookings::select()->where('user_id',$user_id)->where('id',$booking_id)->get();
            // $booking->payment_status = 'paid';
            // return $booking;

            $paid=Bookings::where('user_id', $user_id)
                    ->where('id', $booking_id)
                    ->update(['payment_status' => $paid_status['paid_status']]);

            if($paid){
                return response()->json(['message'=>'Successfully Paid','status'=>'200'],200);
            }else{
                return response()->json(['message'=>'Failed to Paid','status'=>'201'],201);
            }
            // if($booking->save()){
            //     return response()->json(['message'=>'Successfully Paid','status'=>'200'],200);
            // }else{
            //     return response()->json(['message'=>'Failed to Paid','status'=>'201'],201);
            // }
        }
        
    }

    public function createTravel(Request $request){
        $usertype=Auth::user()->type;
        if($usertype=='admin'){
            $travel_name=$request->input('travel_name');
            $type=$request->input('type');
            $number=$request->input('number');
            $from =$request->input('from');
            $to=$request->input('to');
            $amount=$request->input('amount');
            $time=$request->input('time');
            $air_condition=$request->input('ac');
            $image=$request->input('image');

            $travel=new Travel();
            $travel->name=$travel_name;
            $travel->type=$type;
            $travel->number=$number;
            $travel->from=$from;
            $travel->to=$to;
            $travel->amount=$amount;
            $travel->time=$time;
            $rand_num=rand(10000,99999);
            $travel->app_code=$rand_num;

            $sid = env('sid');
            $token = env('token');
            $client = new Client($sid, $token);
            // return $client;
            $msg="Your bus registered successfully. your secret code is ".$rand_num;
            $client->messages->create(
                // the number you'd like to send the message to
                env('sendTo'),
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => env('sendFrom'),
                    // the body of the text message you'd like to send
                    'body' => $msg
                )
            );
            
            if($air_condition==true){
                $travel->air_conditioner=true;
            }else if($air_condition==false){
                $travel->air_conditioner=false;
            }
            $travel->image=$image;
            // return $travel;
            if($travel->save()){
                return response()->json(['message'=>'Travel Created Successfully','status'=>'200'],200);
            }else{
                return response()->json(['message'=>'Travel Creation failed','status'=>'201'],201);
            }
        }
        

    }
    public function editTravel(Request $request){
        $usertype=Auth::user()->type;
        if($usertype=='admin'){
            $travel_id=$request->input('travel_id');
            $travel_name=$request->input('travel_name');
            $type=$request->input('type');
            $number=$request->input('number');
            $from =$request->input('from');
            $to=$request->input('to');
            $amount=$request->input('amount');
            $time=$request->input('time');
            $air_condition=$request->input('ac');
            $image=$request->input('image');

            
            // return $travel;
            // return $travel;
            if($air_condition==1){
                $air_conditioner=true;
            }else if($air_condition==0){
                $air_conditioner=false;
            }
            
            $travel=Travel::find($travel_id);

            $travel->name=$travel_name;
            $travel->type=$type;
            $travel->number=$number;
            $travel->from=$from;
            $travel->to=$to;
            $travel->amount=$amount;
            $travel->time=$time;
            if($air_condition==true){
                $travel->air_conditioner=true;
            }else if($air_condition==false){
                $travel->air_conditioner=false;
            }
            // $travel->image=$image;

            if($travel->save()){
                return response()->json(['message'=>'Travel Updated Successfully','status'=>'200'],200);
            }else{
                return response()->json(['message'=>'Travel Updation failed','status'=>'201'],201);
            }
        }

    }
    public function deleteTravel(Request $request){
        $usertype=Auth::user()->type;
        if($usertype=='admin'){
            $travel_id=$request->input('travel_id');
            $travel=Travel::select()->where('id',$travel_id);
            if($travel->delete()){
                return response()->json(['message'=>'Travel Deleted Successfully','status'=>'200'],200);
            }else{
                return response()->json(['message'=>'Travel Deletion failed','status'=>'201'],201);
            }
        }
    }
}


