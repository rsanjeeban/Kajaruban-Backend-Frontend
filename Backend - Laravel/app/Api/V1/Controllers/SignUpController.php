<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use Validator;

class SignUpController extends Controller
{
    public function signUp(Request $request, JWTAuth $JWTAuth)
    {
        $validator = Validator::make($request->all(), [
            'email'            => 'required|email|max:150|unique:user_datas,email',
            'phone'            => 'required|unique:user_datas,phone|regex:/[0-9]{10}/',
        ]);
        if ($validator->fails()) return response()->json(['error'=>"Mobile or Email is already exists"], 200);

        $user = new User($request->all());
        if(!$user->save()) {
            throw new HttpException(500);
        }

        if(!Config::get('boilerplate.sign_up.release_token')) {
            return response()->json([
                'status' => 'ok'
            ], 201);
        }

        $token = $JWTAuth->fromUser($user);
        return response()->json([
            'status' => 'ok',
            'token' => $token
        ], 201);
    }
}
