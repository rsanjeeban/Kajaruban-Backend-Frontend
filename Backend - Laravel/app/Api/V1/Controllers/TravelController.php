<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\Travel;
use Illuminate\Http\Request;


class TravelController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewTravelsList()
    {
        $travel_list=Travel::all();
        return $travel_list;
    }
    
    public function viewTravel(Request $request){
        $id=$request->input('id');
        $travel=Travel::find($id);
        return $travel;
    }
    public function viewBusDetails($bus_id)
    {
        $bus_details=Travel::select()->where('id',$bus_id)->get();
        return $bus_details[0];
    }
    
    public function filterBus(Request $request)
    {
        // return $request;
        $from=$request->only('from');
        $to=$request->only('to');
        $date=$request->only('date');
        $buses=Travel::select()->where('from',$from)
                                    ->where('to',$to)
                                    ->get();
        if($buses->count()!=0){
            return $buses;
        }else{
            return response("There are no any travels found",500);
        }
    }
    public function activeApp($code){
        $code=Travel::select()->where('app_code',$code)->get();
        if($code->count()!=0){
            return response()
            ->json([
                'status' => '200',
                'message' => 'Code Correct',
                'code'=>$code[0]
            ],200);
        }else{
            return response()
            ->json(['status' => '201','message' => 'Wrong Code'],201);
        }
    }
    public function setLocation(Request $request){
    	$bus_id= $request->input('userId');
    	$latitute=$request->input('latitute');
    	$longitude=$request->input('longitude');

        if($longitude && $latitute){
            $travelBus=Travel::find($bus_id);
            $travelBus->latitude=$latitute;
            $travelBus->longitude=$longitude;
            if($travelBus->save()){
                return response()->json([
                    'status' => '200',
                    'message' => 'Location stored successfully',
                    'bus_data'=> $travelBus
                ],200);
            }else{
                return response()->json([
                    'status' => '201',
                    'message' => 'Location failed to store',
                ],201);
            }            
        }else{
            return response()->json([
                'status' => '201',
                'message' => 'Null data not allowable',
            ],201);   
        }
		
    }
}
