<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TravelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Travels')->insert([
            'name' => 'KTR Travels',
            'type' => 'bus',
            'number' => 'GH12520',
            'from' => 'Jaffna',
            'to' => 'Colombo',
            'amount' => '1300',
            'time' => '7.30 PM',
            'air_conditioner' => true,
            'app_code'=>'1111',
            'latitude'=>'',
            'longitude'=>'',
            'image' => 'https://travelwirenews.com/wp-content/uploads/2018/05/flixbus.png'
        ]);
        DB::table('Travels')->insert([
            'name' => 'Happy Travels',
            'type' => 'bus',
            'number' => 'KJ36658',
            'from' => 'Jaffna',
            'to' => 'Colombo',
            'amount' => '1500',
            'time' => '7.30 PM',
            'air_conditioner' => false,
            'app_code'=>'2222',
            'latitude'=>'',
            'longitude'=>'',
            'image' => 'http://tristatetravel.com/wp-content/blogs.dir/28938/files/2017/12/dl_1-4-2c4.jpg'

        ]);
        DB::table('Travels')->insert([
            'name' => 'Dingo Travels',
            'type' => 'bus',
            'number' => 'ZG15859',
            'from' => 'Jaffna',
            'to' => 'Colombo',
            'amount' => '1700',
            'time' => '7.30 PM',
            'air_conditioner' => true,
            'app_code'=>'3333',
            'latitude'=>'',
            'longitude'=>'',
            'image' => 'https://i0.wp.com/lacartevintage.com/wp-content/uploads/2018/04/traveling-Europe-by-bus.jpg?fit=1000%2C669&ssl=1'
        ]);
        DB::table('Travels')->insert([
            'name' => 'Tofee Travels',
            'type' => 'bus',
            'number' => 'ABC25565',
            'from' => 'Jaffna',
            'to' => 'Colombo',
            'amount' => '2200',
            'time' => '7.30 PM',
            'air_conditioner' => true,
            'app_code'=>'4444',
            'latitude'=>'',
            'longitude'=>'',
            'image' => 'https://brazilbustravel.com/blog/wp-content/uploads/2017/09/bus-in-brazil.png'
        ]);
    }
}
