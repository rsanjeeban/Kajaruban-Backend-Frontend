<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class userTableSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_datas')->insert([
            'name' => 'Admin',
            'phone' => '0212221212',
            'email' => 'admin@admin.com',
            'type' => 'admin',
            'password'=>Hash::make('1234')
        ]);
    }
}
