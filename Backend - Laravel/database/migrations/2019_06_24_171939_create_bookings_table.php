<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_id');
            // $table->foreign('vehicle_id')->references('id')->on('travels');
            $table->integer('user_id');
            // $table->foreign('user_id')->references('id')->on('user_datas');
            $table->string('date');
            $table->string('time');
            $table->enum('payment_status',['paid','unpaid'])->default('unpaid');
            $table->enum('payment_type',['cash','card']);
            $table->integer('amount');
            $table->string('seat_numbers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
