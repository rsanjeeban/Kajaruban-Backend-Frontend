// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  // BaseUrl:'http://mi-linux.wlv.ac.uk/~1731684/finalproject/public/api/',
  BaseUrl:'http://localhost:8000/api/',
  // serverBaseUrl:'/~1731684/bus',
  serverBaseUrl:'',
  logoBaseUrl:'',
  routeUrl:'',
  firebase:{
    apiKey: "AIzaSyAU13ulRQwP25RQFTAg9H9JhWShksbmZow",
    authDomain: "demoproject-3a982.firebaseapp.com",
    databaseURL: "https://demoproject-3a982.firebaseio.com",
    projectId: "demoproject-3a982",
    storageBucket: "demoproject-3a982.appspot.com",
    messagingSenderId: "777544358449",
    appId: "1:777544358449:web:fef0b81ece440e1e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
