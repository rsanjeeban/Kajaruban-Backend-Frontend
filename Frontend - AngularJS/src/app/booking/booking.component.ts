import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {ActivatedRoute} from '@angular/router';
// import { setTimeout } from 'timers';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {

  loading:any=false
  bus_id:any
  busData:any={
    air_conditioner:'',
    name:''
  }
  seats:any=[]
  seletedDate:any
  selected_seats:any[]=[]
  payment_types=["Cash","Card"]
  selectedPaymentType
  message:any={
    failed:'',
    success:''
  }
  booked_seats:any=[]

  constructor(private HttpClient:HttpClient,private route:ActivatedRoute,private router:Router) {
    this.bus_id=route.snapshot.params.id
    this.loadBusDetails(this.bus_id);
    for(let x=1;x<=40;x++){
      let seat={"number":x,color:''}
      this.seats.push(seat)
    }
    // this.loadBookedSeats(this.bus_id,this.selectedPaymentType)

  }

  ngOnInit() {
  }

  loadBusDetails(bus_id){
    console.log(this.route)
    this.loading=true
    let self=this
    this.HttpClient.get(environment.BaseUrl+'viewBus/'+bus_id)
    .subscribe(
      (data:any[])=>{
        this.busData=data
        self.loading=false

      },(err=>{
        self.loading=false
      })
    )
  }

  loadBookedSeats(bus_id,date){
    let self=this
    let formData=new FormData();
    let token=localStorage.getItem('qb_token')
    formData.append('vehicle_id',bus_id)
    formData.append('date',this.convertDate(date))
    formData.append('token',token)
    this.HttpClient.post(environment.BaseUrl+'GetBookedSeats',formData)
    .subscribe(
      (data:any[])=>{
        self.loading=false
        if(data['status']==200){
          self.booked_seats=data['seats']
        }
        this.seats=[]
        for(let x=1;x<=40;x++){
          let seat={"number":x,color:''}
          this.seats.push(seat)
        }
        this.selected_seats=[]
        for(let x=0;x<self.booked_seats.length;x++){
          console.log(self.booked_seats[x])
          let seat={"number":self.booked_seats[x],color:'black'}
          self.seats[self.booked_seats[x]-1]=seat
        }

      },(err=>{
        self.loading=false
      })
    ),err=>{
      if(err['error']['status_code']==401){
        let token=localStorage.getItem('qb_token')
        // window.location.href = environment.serverBaseUrl+'/';
        this.router.navigate(['/'])
      }
    }
  }

  BookSeat(seat_number){
    console.log(seat_number)
    if(this.seats[seat_number-1].color==""){
      let booked_seat={number:seat_number,'color':'red'}
      this.seats[seat_number-1]=booked_seat
      this.selected_seats.push(seat_number)
    }else if(this.seats[seat_number-1].color=="red"){
      let booked_seat={number:seat_number,'color':''}
      this.seats[seat_number-1]=booked_seat
      var index = this.selected_seats.indexOf(seat_number);
      if (index !== -1) this.selected_seats.splice(index, 1);
    }else if(this.seats[seat_number-1].color=="black"){

    }
  }

  BookTravel(travelId,price,time){
    if(this.seletedDate){
      if(this.selected_seats.length!=0){
          let formData=new FormData();
          formData.append('travel_id',travelId)
          let userid=localStorage.getItem('qb_userid')
          formData.append('userid',userid)
          let totalAmount:number=price*this.selected_seats.length
          formData.append('amount',totalAmount.toString())
          formData.append('no_of_seats',this.selected_seats.length.toString())
          formData.append('payment_type','cash')
          formData.append('seat_numbers','['+this.selected_seats.toString()+']')
          formData.append('date',this.convertDate(this.seletedDate))
          formData.append('time',time)
          let token=localStorage.getItem('qb_token')
          formData.append('token',token)
          console.log(userid,travelId,this.selectedPaymentType,this.selected_seats)
          let self=this
          this.loading=true
          this.HttpClient.post( environment.BaseUrl+'book',formData)
          .subscribe(
            (data:any[])=>{
              self.loading=false

              if(data['status']=='200'){
                setTimeout(() => {
                  // window.location.href = environment.serverBaseUrl+'/dashboard';
                    self.router.navigate(['/dashboard'])

                }, 200);
                self.message.success='Successfully Booked';
              }else{
                self.message.failed='Booking Failed.';
              }
            },(err=>{
              self.loading=false
              self.message.failed='Booking Failed.';
            })
          )
      }else{
        this.message.failed='Please choose your seats';
      }

    }else{
      this.message.failed='Please choose the date';
    }
  }

  convertDate(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }
}
