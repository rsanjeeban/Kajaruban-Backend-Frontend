import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminpanel',
  templateUrl: './adminpanel.component.html',
  styleUrls: ['./adminpanel.component.scss']
})
export class AdminpanelComponent implements OnInit {

  user_data:any={
    name:''
  }
  user_bookings:any=[]
  travels_list:any=[]
  AddTravelMessage:any={
    success:'',
    failed:''
  }
  EditTravelMessage:any={
    success:'',
    failed:''
  }
  message:any={
    success:'',
    failed:''
  }
  loading:boolean=false
  loadingAddTravel:boolean=false
  loadingEditTravel:boolean=false

  Travel:any={
    trave_id:'',
    travel_name:'',
    type:'',
    number:'',
    from:'',
    to:'',
    amount:'',
    time:'',
    ac:'',
    image:''
  }
  editStatus:boolean=false
  editFields:any={
    travel_name:'',
    type:'',
    number:'',
    from:'',
    to:'',
    amount:'',
    time:'',
    ac:''
  }


  constructor(private http:HttpClient,private router:Router) {
    this.loadUserData();
    this.GetTravelsList()
  }

  ngOnInit() {
  }

  editTravelShow(travel){
    this.editStatus=true
    this.editFields.travel_id=travel.id
    this.editFields.travel_name=travel.name
    this.editFields.type=travel.type
    this.editFields.number=travel.number
    this.editFields.from=travel.from
    this.editFields.to=travel.to
    this.editFields.amount=travel.amount
    this.editFields.time=travel.time
    this.editFields.ac=travel.air_conditioner
  }
  updateTravel(){
    this.EditTravelMessage.success=''
    this.EditTravelMessage.failed=''
    // EditTravelMessage
    this.loadingEditTravel=true
    console.log("updateTravel")
    this.loading=false

    let token=localStorage.getItem('qb_token')
    let formData=new FormData();
    formData.append('token',token)
    formData.append('travel_id',this.editFields.travel_id)
    formData.append('travel_name',this.editFields.travel_name)
    formData.append('type',this.editFields.type)
    formData.append('number',this.editFields.number)
    formData.append('from',this.editFields.from)
    formData.append('to',this.editFields.to)
    formData.append('amount',this.editFields.amount)
    formData.append('time',this.editFields.time)
    formData.append('ac',this.editFields.ac)
    let self=this
    this.http.post(environment.BaseUrl+'editTravel',formData)
    .subscribe((data) => {
      self.loadingEditTravel=false
      if(data['status']==200){
        self.editStatus=false;
        self.EditTravelMessage.success="Travel update successfully"
        setTimeout(function(){ self.EditTravelMessage.success=""}, 1500);
      }
      self.GetTravelsList();
    }),err=>{
      self.EditTravelMessage.failed="Travel updation failed"
      setTimeout(function(){ self.EditTravelMessage.failed=""}, 1500);
    }
  }

  removeTravel(travel_id){
    console.log(travel_id)
    let token=localStorage.getItem('qb_token')
    let formData=new FormData();
    formData.append('token',token)
    formData.append('travel_id',travel_id)
    this.http.post(environment.BaseUrl+'deleteTravel',formData)
    .subscribe((data) => {
      // self.loading=false
      // let user_id=localStorage.getItem('qb_userid')
      this.editStatus=false
      this.GetTravelsList();
    })
  }

  AddTravel(){
    this.loadingAddTravel=true
    this.AddTravelMessage.failed=''
    this.AddTravelMessage.success=''
    let token=localStorage.getItem('qb_token')
    let formData=new FormData();
    formData.append('token',token)
    formData.append('travel_name',this.Travel.travel_name)
    formData.append('type',this.Travel.type)
    formData.append('number',this.Travel.number)
    formData.append('from',this.Travel.from)
    formData.append('to',this.Travel.to)
    formData.append('amount',this.Travel.amount)
    formData.append('time',this.Travel.time)
    formData.append('ac',this.Travel.ac)
    formData.append('image',this.Travel.image)
    let self=this
    this.http.post(environment.BaseUrl+'createTravel',formData)
    .subscribe((data) => {
      self.loadingAddTravel=false;
      self.AddTravelMessage.success='Travel Successfully Created';
      setTimeout(function(){ self.AddTravelMessage.success=""}, 1500);
      self.GetTravelsList()
    }),err=>{
      self.AddTravelMessage.failed='Travel failed to create';
      setTimeout(function(){ self.AddTravelMessage.failed=""}, 1500);
    }
  }

  GetUserBookings(user_id,formData){
    // this.loading=true
    formData.append('user_id',user_id)
    let self=this
    this.http.post(environment.BaseUrl+'GetAllBookings',formData)
    .subscribe((data) => {
      this.user_bookings=data
      self.loading=false
    })
  }

  GetTravelsList(){
    this.loading=true
    // formData.append('user_id',user_id)
    let self=this
    let formData=new FormData();
    let token=localStorage.getItem('qb_token')
    formData.append('token',token)
    this.http.post(environment.BaseUrl+'GetAllTravels',formData)
    .subscribe((data) => {
      self.travels_list=data
      self.loading=false
    })
  }

  setPaid(userId,bookingId,paymentStatus){
    this.loading=true
    this.message.failed=''
    this.message.success=''
    let token=localStorage.getItem('qb_token')
    let formData=new FormData();
    formData.append('token',token)
    formData.append('user_id',userId)
    formData.append('booking_id',bookingId)
    formData.append('paid_status',paymentStatus)
    let self=this
    this.http.post(environment.BaseUrl+'setPaid',formData)
    .subscribe((data) => {
      self.loading=false
      self.message.success="Successfully "+paymentStatus

      setTimeout(function(){ self.message.success=""}, 1500);

      this.GetUserBookings(userId,formData)
    }),err=>{
      self.loading=false
      self.message.failure="Failed to "+paymentStatus
      setTimeout(function(){ self.message.failure=""}, 1500);
    }
  }

  cancelBooking(booking_id){
    this.message.success=''
    this.message.failed=''
    this.loading=true
    let token=localStorage.getItem('qb_token')
    let formData=new FormData;
    formData.append('token',token);
    formData.append('booking_id',booking_id);
    let self=this
    this.http.post(environment.BaseUrl+'cancelBooking',formData)
    .subscribe((data) => {
      self.loading=false
      self.message.success="Booking successfully cancelled"
      setTimeout(function(){ self.message.success=""}, 1500);
      self.GetUserBookings(this.user_data.id,formData)
    }),(err=>{
      localStorage.removeItem('qb_token')
      // window.location.href = environment.serverBaseUrl+'/login';
      this.router.navigate(['/login']);
      self.message.failed="Cancellation failed"
      setTimeout(function(){ self.message.failed=""}, 1500);
    })
  }

  loadUserData(){
    // debugger
    let self=this
    let params=new HttpParams();
    // headers.append('Access-Control-Allow-Origin',"*");
    let token=localStorage.getItem('qb_token')
    // headers.append('Authorization',token);
    let formData=new FormData;
    formData.append('token',token);

    this.http.post(environment.BaseUrl+'auth/me',formData)
    .subscribe((data) => {
      self.user_data=data
      formData.append('user_id',data['id'])
      localStorage.setItem('qb_userid',data['id']);
      self.GetUserBookings(data['id'],formData);
    }),(err=>{
      localStorage.removeItem('qb_token')
      // window.location.href = environment.serverBaseUrl+'/login';
      this.router.navigate(['/login']);

    })

  }

  viewMap(travel_id){
    console.log(travel_id)
  }

}
