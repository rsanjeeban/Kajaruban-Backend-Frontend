import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  appTitle:string="Hello Bus";
  loginStatus:boolean=true
  user_type:string
  dashboard_link:string
  logoUrl=environment.logoBaseUrl+"/assets/images/logo.png"

  constructor(private HttpClient:HttpClient,private router:Router) {
    let token=localStorage.getItem('qb_token');
    let user_type=localStorage.getItem('user_type');
    this.user_type=user_type

    if(user_type=='admin'){
      this.dashboard_link='/adpanel'
    }else if(user_type=='user'){
      this.dashboard_link='/dashboard'
    }
    if(token){
      this.loginStatus=true
    }else{
      this.loginStatus=false
    }

  }


  ngOnInit() {
  }

  logout(){
    console.log("logout")
    let self=this;
    let token=localStorage.getItem('qb_token')
    let formData=new FormData;
    formData.append('token',token)
    localStorage.removeItem('qb_token')
    this.HttpClient.post(environment.BaseUrl+'auth/logout',formData)
    .subscribe(
      (data:any[])=>{
        // window.location.href = environment.serverBaseUrl+'/';
        self.router.navigate(['/']);
      },(err=>{
        // window.location.href = environment.serverBaseUrl+'/';
        self.router.navigate(['/']);


      }))

  }

}
