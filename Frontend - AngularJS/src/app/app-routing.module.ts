import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { SignupComponent } from "./signup/signup.component";
import { BookingComponent } from "./booking/booking.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AdminpanelComponent } from "./adminpanel/adminpanel.component";
import { MapComponent } from "./map/map.component";
import { AboutComponent } from "./about/about.component";
import { ContactusComponent } from "./contactus/contactus.component";
import { environment } from '../environments/environment'

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'signup',component:SignupComponent},
  {path:'book/:id',component:BookingComponent},
  {path:'dashboard',component: DashboardComponent},
  {path:'map/:id',component: MapComponent},
  {path:'adpanel',component: AdminpanelComponent},
  {path:'about',component: AboutComponent},
  {path:'contact',component: ContactusComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
