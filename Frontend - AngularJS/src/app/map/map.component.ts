import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  lat:number = 51.678418;
  lng:number = 7.809007;
  travel_id:any
  travel_details:any
  constructor(db:AngularFireDatabase,private HttpClient:HttpClient,private route:ActivatedRoute) {
    this.travel_id=route.snapshot.params.id
    let token=localStorage.getItem('qb_token');
    let formData=new FormData();
    formData.append('token',token)
    formData.append('id',this.travel_id)
    let self=this
    this.HttpClient.post( environment.BaseUrl+'viewTravel',formData)
    .subscribe(
      (data:any[])=>{
        self.travel_details=data
        self.lat=data['latitude']
        self.lng=data['longitude']
      })
    //let a=db.collection('location')
    //console.log(a)
    // db.list('/location'+).valueChanges()
    // .subscribe(location=>{
    //   console.log(location);
    //   this.lat=parseInt(location[0])
    //   this.lng=parseInt(location[1])
    // });
  }

  ngOnInit() {
  }
}
