import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private HttpClient:HttpClient,private router:Router) {
    this.loadTravels();
   }

  items=[];
  from_items =["Jaffna","Colombo","Kandy","Mannar","Kilinochchi","Mullaitivu","Trincomalee","Vavuniya","Kurunegala","Ampara","Anuradhapura","Batticaloa"];
  to_items =["Jaffna","Colombo","Kandy","Mannar","Kilinochchi","Mullaitivu","Trincomalee","Vavuniya","Kurunegala","Ampara","Anuradhapura","Batticaloa"];
  loading=false
  fromSelect:string="Jaffna"
  toSelect:string="Colombo"
  seletedDate
  message

  filterTravels(){
    this.message=""
    console.log(this.fromSelect,this.toSelect,this.seletedDate)
    this.loading=true
    let self=this
    let formData=new FormData();
    formData.append('from',this.fromSelect);
    formData.append('to',this.toSelect);
    formData.append('date',this.seletedDate);
    this.HttpClient.post(environment.BaseUrl+'filterBus',formData)
    .subscribe(
      (data:any[])=>{
        this.items=data
        self.loading=false
      },(err=>{
        self.loading=false
        self.message="There are no any travels found"

      })
    )
  }

  ngOnInit() {

  }
  loadTravels(){
    this.loading=true
    let self=this
    this.HttpClient.get(environment.BaseUrl+'viewTravels')
    .subscribe(
      (data:any[])=>{
        this.items=data.slice(0,12)
        self.loading=false

      },(err=>{
        self.loading=false
      })
    )
  }

  RedirectBooking(booking_id){
    let token=localStorage.getItem('qb_token');
    if(token){
      // window.location.href = environment.serverBaseUrl+'/book/'+booking_id;
      this.router.navigate(['/book',booking_id]);
      // this.router.navigate(['/book', { id: 'booking_id' }]);
    }else{
      this.router.navigate(['/login']);
      // window.location.href = environment.serverBaseUrl+'/login';
    }
  }

}
