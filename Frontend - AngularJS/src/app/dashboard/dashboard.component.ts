import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  user_data:any
  user_bookings:any=[]
  message:any={
    success:'',
    failed:''
  }
  loading:boolean=false

  constructor(private http:HttpClient,private router:Router) {
    this.loadUserData();
  }

  ngOnInit() {
  }

  GetUserBookings(user_id,formData){
    this.loading=true
    formData.append('user_id',user_id)
    let self=this
    this.http.post(environment.BaseUrl+'GetUserBookings',formData)
    .subscribe((data) => {
      this.user_bookings=data
      self.loading=false
    })
  }

  cancelBooking(booking_id){
    this.message.success=''
    this.message.failed=''
    this.loading=true
    let token=localStorage.getItem('qb_token')
    let formData=new FormData;
    formData.append('token',token);
    formData.append('booking_id',booking_id);
    let self=this
    this.http.post(environment.BaseUrl+'cancelBooking',formData)
    .subscribe((data) => {
      self.loading=false
      self.message.success="Booking successfully cancelled"
      self.GetUserBookings(this.user_data.id,formData)
    }),(err=>{
      localStorage.removeItem('qb_token')
      // window.location.href = environment.serverBaseUrl+'/login';
      this.router.navigate(['/login'])
      self.message.failed="Cancellation failed"
    })
  }

  loadUserData(){
    // debugger
    let self=this
    let params=new HttpParams();
    // headers.append('Access-Control-Allow-Origin',"*");
    let token=localStorage.getItem('qb_token')
    // headers.append('Authorization',token);
    let formData=new FormData;
    formData.append('token',token);

    this.http.post(environment.BaseUrl+'auth/me',formData)
    .subscribe((data) => {
      self.user_data=data
      formData.append('user_id',data['id'])
      localStorage.setItem('qb_userid',data['id']);
      self.GetUserBookings(data['id'],formData);
    }),(err=>{
      localStorage.removeItem('qb_token')
      // window.location.href = environment.serverBaseUrl+'/login';
      this.router.navigate(['/login'])

    })

  }

}
