import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private HttpClient:HttpClient,
    private router: Router,
    ){}

  ngOnInit() {
  }

  loading=false;
  user={
    email:'',
    password:''
  };
  message={
    success:'',
    failed:''
  }

  Login($window){
    this.loading=true
    this.message.success=''
    this.message.failed=''
    if(this.user.email && this.user.password){
    }else{
      this.message.failed="Please check the fields";
      this.loading=false;
      return;
    }
    let self=this;
    let formData=new FormData;
    formData.append('email',this.user.email);
    formData.append('password',this.user.password);
    this.HttpClient.post( environment.BaseUrl+'auth/login',formData)
    .subscribe(
      (data:any[])=>{
        self.loading=false
        console.log(data)
        if(data['status']=='ok'){
          let user_type=data['user']['type']
          localStorage.setItem('qb_token',data['token']);
          self.message.success='Login Successful';
          if(user_type=="admin"){
            localStorage.setItem('user_type','admin')
            // window.location.href = environment.serverBaseUrl+'/adpanel';
            this.router.navigate(['/adpanel']);
          }else{
            localStorage.setItem('user_type','user')
            // window.location.href = environment.serverBaseUrl+'/dashboard';
            this.router.navigate(['/dashboard']);

          }

        }else{
          self.message.failed='Login Failed.';
        }
      },(err=>{
        self.loading=false
        self.message.failed='Login Failed.';
      })
    )
  }

}
