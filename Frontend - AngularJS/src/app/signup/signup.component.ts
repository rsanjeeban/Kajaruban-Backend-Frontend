import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  user={
    name:'',
    email:'',
    mobile:'',
    password:''
  };
  loading=false
  message={
    success:'',
    failed:''
  }

  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
  }

  Signup(){
    this.loading=true
    console.log("Signup")
    console.log(this.user)
    let self=this;
    let formData=new FormData;
    formData.append('name',this.user.name);
    formData.append('email',this.user.email);
    formData.append('phone',this.user.mobile);
    formData.append('password',this.user.password);
    this.http.post(environment.BaseUrl+'auth/signup',formData)
    .subscribe(
      (data:any[])=>{
        debugger
        self.loading=false
        console.log(data)
        if(data['status']=='ok'){
          self.message.success='Account successfully created';
          // localStorage.setItem('qb_token',data['token']);
          // window.location.href = environment.serverBaseUrl+'/login';
          self.router.navigate(['/login']);
        }else{
          self.message.failed='Account creation failed';
        }
      },(err=>{
        self.loading=false
        self.message.failed='Account creation failed';
      })
    )
  }
}
